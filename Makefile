# Makefile for ah

TARGET = ah.exe
DESTINATION = ${HOME}/bin

all: ${TARGET}

${TARGET}: ah.c
	gcc -o $@ $<

clean:
	rm -f *~ *.exe *.obj ah.ncb

install: ${TARGET}
	install ${TARGET} ${DESTINATION}